<?php

namespace maxlen\emailvalidator\helpers;

/**
 * Validate emails.
 *
 * You can check:
 * - is it email?
 * - get email-adresses from string
 * - does domain provider free?
 * - does domain from email exist?
 * etc.
 */
class EmailValidator
{
    const EMAIL_PATTERN1 = '/[a-zA-Z\d._%+-\\+]+@[a-zA-Z\d.-]+\.[a-zA-Z]{2,4}\b/i';
    const EMAIL_PATTERN2 = "/^[a-z0-9_-]{1,20}@(([a-z0-9-]+\.)+(com|net|org|mil|edu|gov|arpa|info|biz|inc|name|[a-z]{2})|[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3})$/is";
    
    /**
     * Path to file which containes list of free provider domains
     * @var string
     */
    const FREE_PROVIDERS_PATH = '/../data/free_mail_domains.txt';
    
    /**
     * Match pattern of email
     * @var string
     */
    public static $emailPatternVer = self::EMAIL_PATTERN1;
    
    /**
     * List of exceptions which shouldn't containe email
     * @var array
     */
    public static $exceptions = ['inString' => ['mailto:', '#'], 'end' => ['.png', '.jpg', '.gif']];
    
    /**
     * Checks for domain exists
     * @var boolean 
     */
    public static $domainExists = false;
    
    /**
     * Checks for free provider from email
     * @var boolean 
     */
    public static $shouldBeNotFreeProvider = false;
    
    /**
     * Is current string = valid email (this is main rule)
     * @param array $params - [
     *      'exceptions' => ['mailto:', '#', 'mail.ru' ... ],
     *      'emailPatternVer' => EmailValidator::EMAIL_PATTERN1, (default)
     *      'domainExists' => false, (default) - should we check - is domain exist?,
     *      'shouldBeNotFreeProvider' => false, (default)
     *          if parameter = true: checking for free provider
     * ]
     */
    public static function validate($email, $params = [])
    {
        self::setParams($params);
        
        $email = trim($email);
        $match = preg_match(self::$emailPatternVer, $email);
        if ($match == 0 || $match === false) {
            return false;
        }
        
        if (self::$domainExists && !self::domainExists($email)) {
            return false;
        }
        
        if (self::$shouldBeNotFreeProvider && self::isFreeEmailProvider($email)) {
            return false;
        }
        
        if (self::inExceptions($email)) {
            return false;
        }
        
        return true;
    }
    
    /**
     * Is current string - valid email?
     * v.2
     * @param string $email
     * @return boolean
     */
    public static function validate2($email)
    {
       // remove spaces and unnessesary chars (security)
       $email=trim(preg_replace('/[^\x20-\xFF]/','',@strval($email)));
       
       if (strlen($email) == 0) {
           return false;
       }
       
       if (!preg_match(self::EMAIL_PATTERN2,$email)) {
           return false;
       }
       return true;
    }
    
    /**
     * Get emails from string
     * @param string $string
     * @param array $params - [
     *      'exceptions' => ['mailto:', '#', 'mail.ru' ... ],
     *      'emailPatternVer' => EmailValidator::EMAIL_PATTERN1
     *      'domainExists' => false, (default) - should we check - is domain exist?,
     *      'shouldBeNotFreeProvider' => false, (default)
     *          if parameter = true: checking for free provider
     * ]
     * @return array
     */
    public static function emailsFromString($string, $params = [])
    {
        $emailsRes = [];
        self::setParams($params);
        
        $string = strtolower($string);
        $string = preg_replace('#[^a-zA-Z0-9@\.\-_]+#Usi', ' ', $string);
        $string = preg_replace('[@(\s)*]', '@', $string);
        preg_match_all(self::$emailPatternVer, $string, $emails);

        if (!isset($emails[0]) || empty($emails[0])) {
            return $emailsRes;
        }
        
        foreach ($emails[0] as $email) {
            $email = strtolower($email);
            
            if (self::inExceptions($email)) {
                continue;
            }

            if (self::$domainExists && !self::domainExists($email)) {
                continue;
            }

            if (self::$shouldBeNotFreeProvider && self::isFreeEmailProvider($email)) {
                continue;
            }
            
            $emailsRes[] = $email;
        }



        return self::getUniqEmails($emailsRes);
    }
    
    /**
     * Is domain from email exists
     * @param string $email
     * @param string $record - may be any one of:
     *                  A, MX, NS, SOA, PTR, CNAME,
     *                  AAAA, A6, SRV, NAPTR, TXT or ANY.
     * @return boolean
     */
    public static function domainExists($email, $record = 'MX'){
        $domain = self::getDomainFromEmail(trim($email));
        return ($domain) ? checkdnsrr($domain, $record) : false;
        // also you can try PHP-functions like:
        // dns_get_record(), dns_get_mx(), getmxrr() etc. instead checkdnsrr
    }
    
    /**
     * Check validator pattern by isset
     * @param string $version
     * @return boolean
     */
    public static function checkValidatorVer($version)
    {
        return in_array($version, [self::EMAIL_PATTERN1, self::EMAIL_PATTERN2]);
    }
    
    /**
     * Set params processing
     * @param array $params
     */
    public static function setParams($params) 
    {
        if (empty($params)) {
            return;
        }
        
        if (isset($params['emailPatternVer']) && is_string($params['emailPatternVer'])
            && self::checkValidatorVer($params['emailPatternVer'])) {
            self::$emailPatternVer = $params['emailPatternVer'];
        }

        if (isset($params['exceptions']) && is_array($params['exceptions'])) {
            self::$exceptions = $params['exceptions'];
        }
        
        if (isset($params['domainExists']) && is_bool($params['domainExists'])) {
            self::$domainExists = $params['domainExists'];
        }
        
        if (isset($params['shouldBeNotFreeProvider']) && is_bool($params['shouldBeNotFreeProvider'])) {
            self::$shouldBeNotFreeProvider = $params['shouldBeNotFreeProvider'];
        }
    }
    
    /**
     * Checking: is domain provider free?
     * @param string $email
     * @return boolean
     */
    public static function isFreeEmailProvider($email)
    {
        $email = trim($email);
        $filePath = __DIR__ . self::FREE_PROVIDERS_PATH;
        if (file_exists($filePath)
            && ($handle = fopen($filePath, "r"))) {
            while (($freeDomain = fgets($handle)) !== false) {
                $domain = self::getDomainFromEmail($email);
                if ($domain && $domain == trim($freeDomain)) {
                    return true;
                }
            }
        }
        return false;
    }
    
    /**
     * Returns domain part from email
     * @param string $email
     * @return string
     */
    public static function getDomainFromEmail($email)
    {
        list($user, $domain) = explode('@', trim($email));
        return (!is_null($domain)) ? $domain : false;
    }
    
    /**
     * Checking: is email match exceptions?
     * @param string $email
     * @return boolean
     */
    public static function inExceptions($email)
    {
        if (!isset(self::$exceptions) || empty(self::$exceptions)) {
            return false;
        }
        
        foreach (self::$exceptions as $group => $groupExp) {
            foreach ($groupExp as $exp) {
                switch ($group) {
                    case 'inString':
                        if (strstr($email, $exp)) {
                            return true;
                        }
                        break;
                    case 'end':
                        if (preg_match("/{$exp}$/", $email)) {
                            return true;
                        }
                        break;
                    default:
                        break;
                }

            }
        }
        return false;
    }

    public static function getUniqEmails($emailsRes)
    {
        $result = $resultTmp = [];
        foreach ($emailsRes as $r) {
            $emailU = strtolower($r);
            if (!in_array($emailU, $resultTmp)) {
                $resultTmp[] = $emailU;
                $result[] = $r;
            }
        }

        return $result;
    }
}
