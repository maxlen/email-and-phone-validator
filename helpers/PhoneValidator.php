<?php

namespace maxlen\emailvalidator\helpers;

/**
 * Validate phones.
 */
class PhoneValidator
{
    const PHONE_PATTERN1 = '/(\+\d{1,2}\s)?\(?\d{3}\)?[\s.-]\d{3}[\s.-]\d{4}/';

    /**
     * Match pattern of phone
     * @var string
     */
    public static $phonePatternVer = self::PHONE_PATTERN1;
    
    /**
     * Get phones from string
     * @param string $string
     * @param array $params - [
     *      'phonePatternVer' => PhoneValidator::PHONE_PATTERN1
     * ]
     * @return array
     */
    public static function phonesFromString($string, $params = [])
    {
        $phonesRes = [];
        self::setParams($params);

        preg_match_all(self::$phonePatternVer, self::toWithoutTags($string), $phones);

        if (!isset($phones[0]) || empty($phones[0])) {
            return $phonesRes;
        }
        
        foreach ($phones[0] as $phone) {
            $phonesRes[] = $phone;
        }

        $phonesRes = self::getUniqPhones($phonesRes);

        return $phonesRes;
    }
    
    /**
     * Check validator pattern by isset
     * @param string $version
     * @return boolean
     */
    public static function checkValidatorVer($version)
    {
        return in_array($version, [self::PHONE_PATTERN1]);
    }
    
    /**
     * Set params processing
     * @param array $params
     */
    public static function setParams($params) 
    {
        if (empty($params)) {
            return;
        }
        
        if (isset($params['phonePatternVer']) && is_string($params['phonePatternVer'])
            && self::checkValidatorVer($params['phonePatternVer'])) {
            self::$phonePatternVer = $params['phonePatternVer'];
        }
    }

    public static function toWithoutTags($string)
    {
        return preg_replace("/\s{2,}/","  ", strip_tags($string));
    }

    public static function getUniqPhones($phonesRes)
    {
        $result = $resultTmp = [];
        foreach ($phonesRes as $r) {
            $phoneNum = preg_replace('/[^0-9]/', '', $r);
            if (!in_array($phoneNum, $resultTmp)) {
                $resultTmp[] = $phoneNum;
                $result[] = $r;
            }
        }

        return $result;
    }
}
