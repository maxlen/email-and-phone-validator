# README #

Email validator / grabber. Very simple to use.

### By this package you can check / get: ###

* get email-adresses from string
* check: is it email?
* check: does domain provider free?
* check: does domain from email exist?
* check: does phone valid?
* etc.

### Installation: ###

add to file composer.json:

```
#!php

"require": {
   "maxlen/email-and-phone-validator" : "dev-master"
},
"repositories": [
   {
      "type": "git",
      "url": "https://bitbucket.org/maxlen/email-and-phone-validator.git"
   },
]...
```

In console:

```
#!php

php composer.phar require maxlen/email-validator
```


### How should I use it: ###

```
#!php
use maxlen\emailvalidator\helpers\EmailValidator;

EmailValidator::validate('spiderman@superbot.com', $params));
EmailValidator::emailsFromString($string, $params));

, where:
$params = [
   'exceptions' => ['ask.', 'linked', '.ru'],
   'shouldBeNotFreeProvider' => true,
   'domainExists' => true,
];
$string - any string (for example: html-code. You can get emails from web-page)

EmailValidator::domainExists($email, $record); // returns boolean

, where:
$record - MX, A, ...

EmailValidator::isFreeEmailProvider($email); // returns boolean
```

etc.